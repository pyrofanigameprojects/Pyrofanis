# Pyrofani #
<a align="center">
 
**About Me**<br>

:electric_plug: i am an electrical engineer student [ :school: UOA ](https://www.uoa.gr/) 

 * Specialized in automation engineering
 
    * Tia Portal
 
    * Wincc
 
    * Scada

:space_invader: I am video-game developer hobbist
 
* you can check my projects [ :joystick: Pyrofanis](https://pyrofanis.itch.io/)

</a>
